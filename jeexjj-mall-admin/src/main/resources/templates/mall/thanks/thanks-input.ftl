<#--
/****************************************************
 * Description: t_mall_thanks的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/thanks/save" id=tabId>
   <input type="hidden" name="id" value="${thanks.id}"/>
   
   <@formgroup title='nick_name'>
	<input type="text" name="nickName" value="${thanks.nickName}" >
   </@formgroup>
   <@formgroup title='username'>
	<input type="text" name="username" value="${thanks.username}" >
   </@formgroup>
   <@formgroup title='money'>
	<input type="text" name="money" value="${thanks.money}" >
   </@formgroup>
   <@formgroup title='info'>
	<input type="text" name="info" value="${thanks.info}" >
   </@formgroup>
   <@formgroup title='通知邮箱'>
	<input type="text" name="email" value="${thanks.email}" >
   </@formgroup>
   <@formgroup title='状态 0待审核 1确认显示  2驳回 3通过不展示'>
	<input type="text" name="state" value="${thanks.state}" check-type="number">
   </@formgroup>
   <@formgroup title='支付方式'>
	<input type="text" name="payType" value="${thanks.payType}" >
   </@formgroup>
   <@formgroup title='关联订单id'>
	<input type="text" name="orderId" value="${thanks.orderId}" >
   </@formgroup>
   <@formgroup title='date'>
	<@date name="date" dateValue=thanks.date  default=true/>
   </@formgroup>
</@input>