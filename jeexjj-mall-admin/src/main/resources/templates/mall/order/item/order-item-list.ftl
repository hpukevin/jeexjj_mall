<#--
/****************************************************
 * Description: t_mall_order_item的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>商品id</th>
	        <th>订单id</th>
	        <th>商品购买数量</th>
	        <th>商品标题</th>
	        <th>商品单价</th>
	        <th>商品总金额</th>
	        <th>商品图片地址</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.itemId}
			</td>
			<td>
			    ${item.orderId}
			</td>
			<td>
			    ${item.num}
			</td>
			<td>
			    ${item.title}
			</td>
			<td>
			    ${item.price}
			</td>
			<td>
			    ${item.totalFee}
			</td>
			<td>
			    ${item.picPath}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/order/item/input/${item.id}','修改t_mall_order_item','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/order/item/delete/${item.id}','删除t_mall_order_item？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>