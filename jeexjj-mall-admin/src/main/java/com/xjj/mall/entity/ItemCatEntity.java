/****************************************************
 * Description: Entity for 商品类目
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ItemCatEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ItemCatEntity(){}
    private Long parentId;//父分类ID=0时代表一级根分类
    private String name;//分类名称
    private Integer status;//状态 1启用 0禁用
    private Integer sortOrder;//排列序号
    private Integer isParent;//是否为父分类 1为true 0为false
    private String icon;//图标
    private String remark;//备注
    private Date created;//创建时间
    private Date updated;//更新时间
    /**
     * 返回父分类ID=0时代表一级根分类
     * @return 父分类ID=0时代表一级根分类
     */
    public Long getParentId() {
        return parentId;
    }
    
    /**
     * 设置父分类ID=0时代表一级根分类
     * @param parentId 父分类ID=0时代表一级根分类
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    
    /**
     * 返回分类名称
     * @return 分类名称
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置分类名称
     * @param name 分类名称
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回状态 1启用 0禁用
     * @return 状态 1启用 0禁用
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * 设置状态 1启用 0禁用
     * @param status 状态 1启用 0禁用
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    /**
     * 返回排列序号
     * @return 排列序号
     */
    public Integer getSortOrder() {
        return sortOrder;
    }
    
    /**
     * 设置排列序号
     * @param sortOrder 排列序号
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    /**
     * 返回是否为父分类 1为true 0为false
     * @return 是否为父分类 1为true 0为false
     */
    public Integer getIsParent() {
        return isParent;
    }
    
    /**
     * 设置是否为父分类 1为true 0为false
     * @param isParent 是否为父分类 1为true 0为false
     */
    public void setIsParent(Integer isParent) {
        this.isParent = isParent;
    }
    
    /**
     * 返回图标
     * @return 图标
     */
    public String getIcon() {
        return icon;
    }
    
    /**
     * 设置图标
     * @param icon 图标
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }
    
    /**
     * 返回备注
     * @return 备注
     */
    public String getRemark() {
        return remark;
    }
    
    /**
     * 设置备注
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置创建时间
     * @param created 创建时间
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回更新时间
     * @return 更新时间
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置更新时间
     * @param updated 更新时间
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.ItemCatEntity").append("ID="+this.getId()).toString();
    }
}

