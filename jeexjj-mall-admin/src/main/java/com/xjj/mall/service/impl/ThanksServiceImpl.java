/****************************************************
 * Description: ServiceImpl for t_mall_thanks
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.common.pojo.DataTablesResult;
import com.xjj.mall.dao.ThanksDao;
import com.xjj.mall.entity.ThanksEntity;
import com.xjj.mall.service.ThanksService;

@Service
public class ThanksServiceImpl extends XjjServiceSupport<ThanksEntity> implements ThanksService {

	@Autowired
	private ThanksDao thanksDao;

	@Override
	public XjjDAO<ThanksEntity> getDao() {
		
		return thanksDao;
	}
	
	
	
	@Override
    public DataTablesResult getThanksListByPage(int page, int size) {

//        DataTablesResult result=new DataTablesResult();
//        TbThanksExample example=new TbThanksExample();
//        if(page<=0) {
//            page = 1;
//        }
//        //PageHelper.startPage(page,size);
//        List<TbThanks> list=tbThanksMapper.selectByExample(example);
//        if(list==null){
//            throw new XmallException("获取捐赠列表失败");
//        }
//        PageInfo<TbThanks> pageInfo=new PageInfo<>(list);
		
		
		List<ThanksEntity> list = thanksDao.findListLimit(null, (page-1)*size, size);

        for(ThanksEntity tbThanks:list){
            tbThanks.setEmail(null);
        }
        DataTablesResult result=new DataTablesResult();
        result.setSuccess(true);
        result.setRecordsTotal(thanksDao.getCount(null));
        result.setData(list);
        return result;
    }
}