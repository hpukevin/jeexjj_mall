/****************************************************
 * Description: ServiceImpl for 商品描述表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.ItemDescEntity;
import com.xjj.mall.dao.ItemDescDao;
import com.xjj.mall.service.ItemDescService;

@Service
public class ItemDescServiceImpl extends XjjServiceSupport<ItemDescEntity> implements ItemDescService {

	@Autowired
	private ItemDescDao itemDescDao;

	@Override
	public XjjDAO<ItemDescEntity> getDao() {
		
		return itemDescDao;
	}
}